\contentsline {chapter}{Acknowledgements}{iii}{Doc-Start}
\contentsline {chapter}{Abstract}{iv}{Doc-Start}
\contentsline {chapter}{List of Tables}{x}{section*.1}
\contentsline {chapter}{List of Figures}{xiv}{section*.2}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}College Alcohol Consumption and NCAA Tournament Participation: the Health Cost of March Madness}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduction}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Empirical Model}{8}{section.2.2}
\contentsline {section}{\numberline {2.3}Data}{13}{section.2.3}
\contentsline {section}{\numberline {2.4}Results}{18}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Primary Specification Regression Results}{18}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Intertemporal Substitution of Drinks}{21}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Effects of One More Round}{22}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Possible Confounding Effects}{23}{subsection.2.4.4}
\contentsline {section}{\numberline {2.5}Conclusion}{26}{section.2.5}
\contentsline {chapter}{\numberline {3}Agency Theory and the Decision to Work From Home}{43}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction}{43}{section.3.1}
\contentsline {section}{\numberline {3.2}Applying Agency Theory to the Problem}{47}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Inducing High Effort}{47}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Monitoring and Wage Variance}{52}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Shifts in the Wage Differential Due to Technology}{54}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}If Low Effort Is Preferred}{56}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Empirical Model}{57}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Estimating the Wage Differential}{57}{subsection.3.3.1}
\contentsline {subsubsection}{Selection Bias of Home-Workers}{59}{equation.3.3.6}
\contentsline {subsection}{\numberline {3.3.2}Estimating the Variance of Wages}{62}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Data}{63}{section.3.4}
\contentsline {section}{\numberline {3.5}Results}{65}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Wage Differential}{65}{subsection.3.5.1}
\contentsline {subsubsection}{Least Squares Analysis}{65}{subsection.3.5.1}
\contentsline {subsubsection}{Common Industries and Occupations}{67}{subsection.3.5.1}
\contentsline {subsubsection}{Heckman Selection Model}{68}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Wage Variance Analysis}{69}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}Conclusion}{70}{section.3.6}
\contentsline {chapter}{\numberline {4}Network Externalities and Friendly Neighbors: When Firms Choose to Invite Competition}{85}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction and Literature Review}{85}{section.4.1}
\contentsline {section}{\numberline {4.2}Theoretical Model}{89}{section.4.2}
\contentsline {section}{\numberline {4.3}Data}{92}{section.4.3}
\contentsline {section}{\numberline {4.4}Empirical Model}{95}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Panel Analysis}{95}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Time Series Analysis}{97}{subsection.4.4.2}
\contentsline {section}{\numberline {4.5}Discussion of Results}{98}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Results of Panel Analysis}{98}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Results of Time Series Analysis}{100}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Alternate Specifications}{102}{subsection.4.5.3}
\contentsline {section}{\numberline {4.6}Conclusion}{104}{section.4.6}
\contentsline {chapter}{Bibliography}{114}{chapter*.36}
\contentsline {chapter}{Appendices}{121}{section*.37}
\contentsline {chapter}{\numberline {A}College Alcohol Consumption and NCAA Tournament Participation: the Health Cost of March Madness}{122}{appendix.A}
\contentsline {section}{\numberline {A.1}Specifications of 3 and 5 Week Response Delays}{122}{section.A.1}
\contentsline {section}{\numberline {A.2}Specifications Based on Seed in NCAA Tournament}{129}{section.A.2}
\contentsline {chapter}{\numberline {B}Agency Theory and the Decision to Work From Home}{136}{appendix.B}
\contentsline {section}{\numberline {B.1}OLS Tables for All Years}{136}{section.B.1}
\contentsline {chapter}{\numberline {C}Network Externalities and Friendly Neighbors: When Firms Choose to Invite Competition}{152}{appendix.C}
\contentsline {section}{\numberline {C.1}Alternate Specifications}{152}{section.C.1}
\contentsline {subsection}{\numberline {C.1.1}Including Lead Years}{152}{subsection.C.1.1}
\contentsline {subsection}{\numberline {C.1.2}Including Proximity Interaction}{156}{subsection.C.1.2}
\contentsline {section}{\numberline {C.2}Diagnostic Plots for Packers' Time Series}{158}{section.C.2}
\contentsline {subsection}{\numberline {C.2.1}Preliminary Plots}{158}{subsection.C.2.1}
\contentsline {subsection}{\numberline {C.2.2}Residual Plots}{160}{subsection.C.2.2}
